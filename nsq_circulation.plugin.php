<?php
/**
 * Plugin Name: NSQ Support for Circulation
 * Plugin URI:
 * Description: Sending data to NSQ related to circulation. 
 * Version: 1.0.0
 * Author: Hendro Wicaksono
 * Author URI: https://github.com/hendrowicaksono
 */

use SLiMS\DB;
use SLiMS\Plugins;

require 'vendor/autoload.php';

$nsqb['mode'] = 'prod';
$nsqb['host'] = '127.0.0.1';
$nsqb['port'] = '4151';
$nsqb['topic'] = 'circulation';
$nsqb['url'] = 'http://'.$nsqb['host'].':'.$nsqb['port'].'/pub?topic='.$nsqb['topic'];

// get plugin instance
$plugin = \SLiMS\Plugins::getInstance();

// registering menus or hook
$plugin->register("circulation_after_successful_transaction", function($data) use (&$nsqb) {
    # Make sure that data sent to NSQ contains transaction(s).
    if ( (isset($data['loan'])) OR (isset($data['return'])) OR (isset($data['extend'])) ) {
        # Getting member data to get member_phone info.
        $member_data = api::member_load(DB::getInstance('mysqli'), $data['memberID']);
        # Tambahkan validasi member_phone disini. Kalau nomer tidak ada atau
        # tidak valid, ga usah kirim ke NSQ.
        if (isset($member_data[0]['member_phone'])) {
            $client = new \GuzzleHttp\Client();
            $_msg['hook'] = 'circulation_after_successful_transaction';
            $_msg['status'] = 'success';
            $_msg['circ_data'] = $data;
            $_msg['member_phone'] = $member_data[0]['member_phone'];
            $msg = json_encode($_msg, JSON_PRETTY_PRINT);
            $response = $client->request('POST', $nsqb['url'], [
                'body' => $msg
            ]);    
        }
    }
});
